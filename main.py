
from typing import NoReturn
from flask import Flask, render_template, request, redirect, session, flash
from flask import Flask, jsonify, request
from flask.helpers import url_for
from flask_pymongo import PyMongo
from flask_cors import CORS
from pymongo.message import _EMPTY
import datetime
from bson import ObjectId



# Instantiation
app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://localhost/pythonreact'
mongo = PyMongo(app)

# Settings
CORS(app)

# Database
db = mongo.db.pythonreact


app.secret_key = b'\x1c$\xf0]\xa5|\xfd\xfd\x98\xdc\xc1\x17\x8e*_/'

"""
Definición de rutas
"""

# Protegida. Solo pueden entrar los que han iniciado sesión


@app.route("/escritorio")
def escritorio():
    return render_template("escritorio.html")

@app.route("/registro")
def registro():
    return render_template("registro.html")    
    
@app.route("/grupos")
def grupos():
    grupo = db.grupo.find()
    return render_template("grupos.html",grupo = grupo)  

@app.route("/foros")
def foros():
    foro = db.foro.find()
    return render_template("foros.html",foro = foro)  

@app.route("/foro_activo/<string:id>",methods=['GET'])
def foro_activo(id):

  foro = db.foro.find_one({"_id": ObjectId(id)})
  coment = db.comentario.find({"idforo": id})
  return render_template("foro_activo.html", foro = foro, coment = coment)   

#crear comentarios
@app.route("/foro_activo/<string:id>",methods=['POST'])
def crearComentario(id):

  cd = db.comentario.insert({
    'idforo': id,
    'comentarioForo': request.form['comentarioForo'],
    'usuario' : session['usuario'],
    'date': datetime.datetime.today()
    
  })

  foro = db.foro.find_one({"_id": ObjectId(id)})
  coment = db.comentario.find({"idforo": id})
  return render_template("foro_activo.html", foro = foro, coment = coment)    

#crear usuario
@app.route('/login', methods=['POST'])
def createUser():

  id = db.users.insert({
    'name': request.form['name'],
    'email': request.form['email'],
    'password': request.form['password']
    
  })
  return redirect("/login")

#crear foro  
@app.route('/foros', methods=['POST'])
def createForo():
  
  id = db.foro.insert({
    'nameForo': request.form['nameForo'],
    'descripcionForo': request.form['descripcionForo'],
    'usuario' : session['usuario'],
    'date': datetime.datetime.today()
   
  })
  return redirect("/foros")



#crear grupo  
@app.route('/grupos', methods=['POST'])
def createGrupo():
  
  id = db.grupo.insert({
    'nameGrupo': request.form['nameGrupo'],
    'descripcionGrupo': request.form['descripcionGrupo'],
    'usuario' : session['usuario'] 
   
  })
  return redirect("/grupos")





# Formulario para iniciar sesión


@app.route("/login")
def login():
    return render_template("login.html")

# Manejar login


@app.route("/hacer_login", methods=["POST"])
def hacer_login():
    correo = request.form["correo"]
    palabra_secreta = request.form["palabra_secreta"]
    # Aquí comparamos. Lo hago así de fácil por simplicidad
    
    

    if db.users.find_one({"email":correo, "password":palabra_secreta}):
        # Si coincide, iniciamos sesión y además redireccionamos
        session["usuario"] = correo
        # Aquí puedes colocar más datos. Por ejemplo
        # session["nivel"] = "administrador"
        id = db.actividad_usuario.insert({
          'usuario' : session['usuario'],
          'horalogin' : datetime.datetime.now()
        
        })
        return redirect("/escritorio")
    else:
        # Si NO coincide, lo regresamos
        flash("Correo o contraseña incorrectos")
        return redirect("/login")


# Cerrar sesión
@app.route("/logout")
def logout():
    id = db.actividad_usuario.insert({
          'usuario' : session['usuario'],
          'horalogout' : datetime.datetime.now()
        
        })
    session.pop("usuario", None)
    return redirect("/login")


# Un "middleware" que se ejecuta antes de responder a cualquier ruta. Aquí verificamos si el usuario ha iniciado sesión
@app.before_request
def antes_de_cada_peticion():
    ruta = request.path
    # Si no ha iniciado sesión y no quiere ir a algo relacionado al login, lo redireccionamos al login
    if not 'usuario' in session and ruta != "/login" and ruta != "/hacer_login" and ruta != "/logout" and ruta != "/registro" and not ruta.startswith("/static"):
        flash("Inicia sesión para continuar")
        return redirect("/login")
    # Si ya ha iniciado, no hacemos nada, es decir lo dejamos pasar


# Iniciar el servidor
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
